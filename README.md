# Polyglot

A proof-of-concept Spigot plugin that utilizes a REST API to support any programming language with HTTPS and JSON support.